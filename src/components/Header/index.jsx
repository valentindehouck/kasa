import {Logo} from '../Logo'
import routes from '../../config/routes'
import {Link, NavLink} from 'react-router-dom'
import './style.scss'

export default function Header() {
    return (
        <header className='header'>
            <Link className='header__link' to={routes.HOME}>
                <Logo type='header'/>
            </Link>

            <nav className='header__navbar'>
                <NavLink to={routes.HOME}
                         className={({isActive}) => isActive ? 'header__link header__link--active' : 'header__link'}>
                    Accueil</NavLink>

                <NavLink to={routes.ABOUT}
                         className={({isActive}) => isActive ? 'header__link header__link--active' : 'header__link'}>
                    À propos</NavLink>
            </nav>
        </header>
    )
}