import './style.scss'
import PropTypes from 'prop-types'

export default function HostAvatar({name, picture}) {
    return (
        <>
            <p className='host_avatar__name'>{name}</p>
            <img className='host_avatar__profile' src={picture} alt={name}/>
        </>
    )
}

HostAvatar.propTypes = {
    name: PropTypes.string,
    picture: PropTypes.string
}