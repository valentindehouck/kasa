import {useState} from 'react'
import './style.scss'
import PropTypes from 'prop-types'

export const Slideshow = ({images}) => {
    const [active, setActive] = useState(0)

    const onNext = () => {
        setActive(active === images.length - 1 ? 0 : active + 1)
    }

    const onPrev = () => {
        setActive(active === 0 ? images.length - 1 : active - 1)
    }

    return (
        <div className='slideshow'>
            {images.map((img, index) =>
                <img key={index}
                     className={`slideshow__slide ${index === active ? 'slideshow__slide--active' : ''}`}
                     src={img} alt=''/>
            )}

            {images.length > 1 &&
                <>
                    <svg className="slideshow__prev" onClick={onPrev} width="96" height="96" viewBox="0 0 96 96"
                         fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M70.04 15.7831L62.92 8.70312L23.36 48.3031L62.96 87.9031L70.04 80.8231L37.52 48.3031L70.04 15.7831Z"
                            fill="white"/>
                    </svg>
                    <svg className="slideshow__next" onClick={onNext} width="96" height="96" viewBox="0 0 96 96"
                         fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M25.96 81.3458L33.04 88.4258L72.64 48.8258L33.04 9.22583L25.96 16.3058L58.48 48.8258L25.96 81.3458Z"
                            fill="white"/>
                    </svg>
                    <p className='slideshow__index'>{active + 1}/{images.length}</p>
                </>
            }
        </div>
    )
}

Slideshow.propTypes = {
    images: PropTypes.arrayOf(PropTypes.string)
}