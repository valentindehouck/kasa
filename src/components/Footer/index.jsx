import './style.scss'
import {Logo} from '../Logo'

export default function Footer() {
    return (
        <footer className='footer'>
            <Logo type='footer'/>

            <p className='footer__copyright'>© 2020 Kasa. All rights reserved</p>
        </footer>
    )
}