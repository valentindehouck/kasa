import './style.scss'
import PropTypes from 'prop-types'

export default function Banner({image, children}) {
    return (
        <div className='banner'
             style={{backgroundImage: `url(${image}), linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3))`}}>
            {children}
        </div>
    )
}

Banner.propTypes = {
    image: PropTypes.string,
    children: PropTypes.node
}