import './style.scss'
import PropTypes from 'prop-types'

export const Tag = ({text}) => {
    return (
        <p className='tag'>{text}</p>
    )
}

Tag.propTypes = {
    text: PropTypes.string
}