import {Link} from 'react-router-dom'
import routes from '../../config/routes'
import './style.scss'

export default function NotFound() {
    return (
        <div className='notfound_page'>
            <h1 className='notfound_page__title'>404</h1>
            <p className='notfound_page__error'>Oups! La page que vous demandez n&#39;existe pas.</p>
            <Link to={routes.HOME} className='notfound_page__redirect'>Retourner sur la page d&#39;accueil</Link>
        </div>
    )
}