import Banner from '../../components/Banner'
import Collapse from '../../components/Collapse'
import './style.scss'
import ImageBanner from '../../assets/background_about.jpg'

export default function About() {
    return (
        <>
            <Banner image={ImageBanner}/>

            <div className='about__container_collapse'>
                <Collapse label="Fiabilité">
                    <p>Les annonces postées sur Kasa garantissent une fiabilité totale. Les photos sont conformes aux
                        logements, et toutes les informations sont régulièrement vérifiées par nos équipes.</p>
                </Collapse>

                <Collapse label="Respect">
                    <p>La bienveillance fait partie des valeurs fondatrices de Kasa. Tout comportement discriminatoire
                        ou de perturbation du voisinage entraînera une exclusion de notre plateforme.</p>
                </Collapse>

                <Collapse label="Service">
                    <p>La bienveillance fait partie des valeurs fondatrices de Kasa. Tout comportement discriminatoire
                        ou de perturbation du voisinage entraînera une exclusion de notre plateforme.</p>
                </Collapse>

                <Collapse label="Sécurité">
                    <p>La sécurité est la priorité de Kasa. Aussi bien pour nos hôtes que pour les voyageurs, chaque
                        logement correspond aux critères de sécurité établis par nos services. En laissant une note
                        aussi bien à l&#39;hôte qu&#39;au locataire, cela permet à nos équipes de vérifier que les standards
                        sont bien respectés. Nous organisons également des ateliers sur la sécurité domestique pour nos
                        hôtes.</p>
                </Collapse>
            </div>
        </>
    )
}