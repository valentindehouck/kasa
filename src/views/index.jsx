import Home from './Home'
import NotFound from './NotFound'
import Housing from './Housing'
import About from './About'

export {
    Home,
    NotFound,
    Housing,
    About,
}