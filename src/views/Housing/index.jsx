import {useEffect} from 'react'
import {useNavigate, useParams} from 'react-router-dom'
import locations from '../../data/locations.json'
import {Slideshow} from '../../components/Slideshow'
import Collapse from '../../components/Collapse'
import './style.scss'
import {Rating} from '../../components/Rating'
import {Tag} from '../../components/Tag'
import HostAvatar from '../../components/HostAvatar'
import routes from '../../config/routes'

export default function Housing() {
    const {id} = useParams()
    const navigate = useNavigate()
    const location = locations.filter(loc => loc.id === id)[0]

    useEffect(() => {
        if (!location) {
            navigate(routes.NOTFOUND)
        }
    }, [location, navigate])

    if (!location) {
        return null
    }

    return (
        <>
            <Slideshow images={location.pictures}/>
            <div className='housing'>
                <div className='housing__reference'>
                    <h1 className='housing__reference_title'>{location.title}</h1>
                    <h2 className='housing__reference_location'>{location.location}</h2>
                    <div className='housing__reference_tags'>
                        {location.tags.map((tag, index) =>
                            <Tag key={index} text={tag}/>
                        )}
                    </div>
                </div>
                <div className='housing__host'>
                    <div className='housing__host_infos'>
                        <HostAvatar name={location.host.name} picture={location.host.picture}/>
                    </div>
                    <div className='housing__host_rating'>
                        <Rating rating={parseInt(location.rating)}/>
                    </div>
                </div>
            </div>
            <div className='housing__collapse_container'>
                <Collapse label='Description'>
                    <p>{location.description}</p>
                </Collapse>
                <Collapse label='Équipements'>
                    <ul>
                        {location.equipments.map((element, index) => <li key={index}>{element}</li>)}
                    </ul>
                </Collapse>
            </div>
        </>
    )
}
