import {Link} from 'react-router-dom'
import './style.scss'
import Banner from '../../components/Banner'
import locations from '../../data/locations.json'
import ImageBanner from '../../assets/background_home.jpg'
import routes from '../../config/routes'

export default function Home() {
    return (
        <>
            <Banner image={ImageBanner}>
                <h1 className='banner__title'>Chez vous, partout et ailleurs</h1>
            </Banner>

            <section className='home__list'>
                {locations.map(({id, cover, title}) => {
                    return (
                        <Link key={id} to={routes.HOUSING + '/' + id} className='home__list_link'>
                            <article className='home__list_element'>
                                <img src={cover} alt={title} className='home__list_element_image'/>

                                <h2 className='home__list_element_title'>{title}</h2>
                            </article>
                        </Link>
                    )
                })}
            </section>
        </>
    )
}

