import {Navigate, Outlet, Route, Routes} from 'react-router-dom'
import {Home, NotFound, Housing, About} from '../views'
import Footer from '../components/Footer'
import Header from '../components/Header'
import routes from './routes'

export default function Router() {
    return (
        <Routes>
            <Route path={routes.HOME} element={<Layout/>}>
                <Route index element={<Home/>}/>
                <Route path={routes.ABOUT} element={<About/>}/>
                <Route path={routes.HOUSING + "/:id"} element={<Housing/>}/>
                <Route path={routes.NOTFOUND} element={<NotFound />}/>
                <Route path="*" element={<Navigate replace to="notfound" />}/>
            </Route>
        </Routes>
    )
}

function Layout() {
    return (
        <>
            <Header/>
            <main>
                <Outlet/>
            </main>
            <Footer/>
        </>
    )
}
