export default {
    HOME: '/',
    HOUSING: 'housing',
    ABOUT: 'about',
    NOTFOUND: 'notfound'
}